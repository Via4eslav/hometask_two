package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AuthPopUp;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ivanov.v on 11.10.2017.
 */
public class RegistrationTest extends BaseConfig {
    public AuthPopUp authorizationPopUp;

    @BeforeMethod
    public void beforeTest(){
        authorizationPopUp = new AuthPopUp();
    }

    @Test
    public void regUserValidData(){
        /**1. Пользователь открыл сайт: ('http://kronas.wezom.net/') */
        open(baseUrl);

        /**2. Пользовател увидел кнопку "Личный кабинет" */
        authorizationPopUp.openPopUpBtnn.should(exist).shouldBe(visible);

        /**3. Пользователь нажал на кнопку "Личный кабинет"*/
        authorizationPopUp.setAuthPopUp();

        /**4. Пользователь увидел что открылся поп-ап. */
        authorizationPopUp.modalWindowAuth.should(exist).shouldBe(visible);

        /**5. Пользователь увидел блок "Регистрация" в открывшемся поп-апе.  */
        authorizationPopUp.registrationDiv.should(exist);

        /**6. Пользователь заполнил поле email (валидными даннымм) */
        authorizationPopUp.setEmailInput("user@user");

        /**7. Пользователь заполнил поле пароль (валидными данными) */
        authorizationPopUp.setPasswordInput("pa$$word");

        /**8. Пользователь отметил чекер "я согласен с условиями исп..." */
        authorizationPopUp.setTermsCheckBox();

        /**9. Пользователь нажал на кнопку "Регистрироваться". */
        authorizationPopUp.setRegistrationButton();

        /**10. Пользователь увидел сообщение об успешной регистрации. */
        authorizationPopUp.confirmEmailMessage.shouldHave(exactText("Письмо для подтверждения регистрации отправлено на E-Mail"));
    }

}
