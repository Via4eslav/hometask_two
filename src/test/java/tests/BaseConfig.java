package tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Configuration.startMaximized;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by ivanov.v on 11.10.2017.
 */
public class BaseConfig {
    final String baseUrl = "http://1:1@kronas.wezom.net";

    @BeforeClass
    public void start() {

        browser = "chrome";
        startMaximized = true;
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
        clearBrowserCookies();
    }

    @AfterClass
    public void tearDown(){
        close();
    }
}
