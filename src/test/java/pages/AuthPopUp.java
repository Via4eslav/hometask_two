package pages;

import com.codeborne.selenide.SelenideElement;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

/**
 * Created by ivanov.v on 11.10.2017.
 */
public class AuthPopUp {
    public SelenideElement
            openPopUpBtnn = $x("/html/body/main/header/div[1]/div[2]/a[2]/span"),
            authPopUp = $x("html/body/main/header/div[1]/div[2]/a[2]"),
            emailInput = $x("html/body/div[2]/div/div[1]/div/div[1]/div/div[7]/div/div[1]/div/input"),
            passwordInput = $x("html/body/div[2]/div/div[1]/div/div[1]/div/div[7]/div/div[2]/div/input"),
            termsCheckBox = $x("html/body/div[2]/div/div[1]/div/div[1]/div/div[7]/div/div[3]/div/label/ins"),
            registrationButton = $x("html/body/div[2]/div/div[1]/div/div[1]/div/div[7]/div/div[4]/div/div"),
            successMessage = $(".wnoty_message"),
            confirmEmailMessage = $x("/html/body/div[2]/div/div[1]/div/div[1]/div/div[7]/div/div[3]/span/span/span/span[2]"),
            modalWindowAuth = $(".mfpPopup.mfpPopup--big"),
            registrationDiv = $(".authBlockForm.form.js-form.js-regForm");


    public void setAuthPopUp(){
        authPopUp.click();
    }
    public void setEmailInput(String email){
        Random r = new Random();
        char a = (char)(r.nextInt(26) + 'a');
        char b = (char)(r.nextInt(26) + 'b');
        emailInput.val(a + email + a + b + ".com");
    }
    public void setPasswordInput(String password){
        passwordInput.val(password);
    }
    public void setTermsCheckBox(){
        termsCheckBox.click();
    }
    public void setRegistrationButton(){
        registrationButton.click();
    }
}
